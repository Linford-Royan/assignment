const { getDateValue } = require("../utils/utils");
const { Records, validate } = require("../models/records-model");

async function recordsController(req, res) {
	// validate if the payload entries are valid
	const { error } = validate(req.body);
	if (error) return res.status(400).send(error.details[0].message);
	// store the query params in a constants
	const minDate = getDateValue(req.body.startDate);
	const maxDate = getDateValue(req.body.endDate);
	const minCount = parseInt(req.body.minCount);
	const maxCount = parseInt(req.body.maxCount);
	// Aggregate method for filtering through the list
	const recordsAgrregatre = Records.aggregate([
		{
			// Set the record structure along with summing the counts array
			$project: {
				totalCount: { $sum: "$counts" },
				key: "$key",
				createdAt: "$createdAt",
			},
		},
		{
			// Apply the filters using query params.
			$match: {
				createdAt: {
					$gte: new Date(minDate),
					$lte: new Date(maxDate),
				},
				totalCount: {
					$gte: minCount,
					$lte: maxCount,
				},
			},
		},
		{
			// Remove the id field from response Object
			$unset: ["_id"],
		},
	]);
	recordsAgrregatre.then((data) => {
		// Initialize the response data to be sent
		const responseToSend = {
			code: 0,
			msg: "",
			records: [],
		};
		// Check if data is available for given inputs after filtering
		if (data && data.length > 0) {
			// If yes send statuscode as 0 with the list of records
			responseToSend.msg = "success";
			responseToSend.records = data;
		} else {
			// if no record is found send statuscode as -1 with corresponding message and records as null.
			responseToSend.code = -1;
			responseToSend.msg = "No records found";
			responseToSend.records = null;
		}
		res.status(200).send(responseToSend);
	});
}

exports.recordsController = recordsController;
