module.exports.asyncHandler = function (handler) {
	return async (req, res) => {
		try {
			await handler(req, res);
		} catch (ex) {
			console.error(err.message, err);
			res.status(500).send("Internal Server Error");
		}
	};
};
