// Utils module for common functions

// function to get date value. This function accepts string and return date value.
function getDateValue(date) {
	if (date) {
		return new Date(date);
	} else {
		return null;
	}
}

exports.getDateValue = getDateValue;
