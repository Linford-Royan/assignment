const { getDateValue } = require("../../utils/utils");
const { Records, validate } = require("../../models/records-model");

describe("GetDateValue", () => {
	it("should return a new Date when input of date string is provided", async () => {
		expect(getDateValue("2022-01-01")).toEqual(new Date("2022-01-01"));
	});
	it("should return null when no input is provided", () => {
		expect(getDateValue()).toBeNull();
	});
});

describe("validate", () => {
	it("should return result with no error field when proper request body is provided", () => {
		const req = {
			startDate: "2016-01-26",
			endDate: "2018-02-02",
			minCount: 2700,
			maxCount: 3000,
		};
		const result = validate(req);
		expect(result.error).toBeUndefined();
	});
	it("should return an error Object-with wrongDate error message when incorrect date request is provided", () => {
		const req = {
			startDate: "abc",
			endDate: "2018-02-02",
			minCount: 2700,
			maxCount: 3000,
		};
		const result = validate(req);
		expect(result.error).toBeDefined();
		expect(result.error.details[0].message).toEqual(`"startDate" must be a valid date`);
	});
	it("should return an error Object-with wrong Date range error message when startDate > endDate request is provided", () => {
		const req = {
			startDate: "2019-02-11",
			endDate: "2018-02-02",
			minCount: 2700,
			maxCount: 3000,
		};
		const result = validate(req);
		expect(result.error).toBeDefined();
		expect(result.error.details[0].message).toEqual(`"startDate" must be less than "ref:endDate"`);
	});
	it("should return an error Object-with wrong count range error message when minCount > maxCount request is provided", () => {
		const req = {
			startDate: "2016-02-11",
			endDate: "2018-02-02",
			minCount: 4700,
			maxCount: 3000,
		};
		const result = validate(req);
		expect(result.error).toBeDefined();
		expect(result.error.details[0].message).toEqual(`"minCount" must be less than ref:maxCount`);
	});
	it("should return an error Object-with wrong count error message when no minCount request  is provided", () => {
		const req = {
			startDate: "2016-02-11",
			endDate: "2018-02-02",
			maxCount: 3000,
		};
		const result = validate(req);
		expect(result.error).toBeDefined();
		expect(result.error.details[0].message).toEqual(`"minCount" is required`);
	});
	it("should return an error Object-with wrong startDate error message when no startDate request  is provided", () => {
		const req = {
			endDate: "2019-02-02",
			minCount: 4700,
			maxCount: 3000,
		};
		const result = validate(req);
		expect(result.error).toBeDefined();
		expect(result.error.details[0].message).toEqual(`"startDate" is required`);
	});
});
