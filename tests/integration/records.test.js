const records = require("../../routes/records");
const request = require("supertest");
let server;

jest.setTimeout(3600000);
describe("/api/records", () => {
	beforeEach(() => {
		// jest.useRealTimers();
		server = require("../../index");
	});
	afterEach(() => {
		server.close();
	});
	it("should return the bad request as null when correct incorrect body is passed", async () => {
		const req = {
			startDate: "2020-01-26",
			endDate: "2018-02-02",
			minCount: 2700,
			maxCount: 3000,
		};
		const res = await request(server).post("/api/records").send(req);
		expect(res.status).toBe(400);
	});
	it("should return the records when correct request body is passed", async () => {
		const req = {
			startDate: "2016-01-26",
			endDate: "2019-02-02",
			minCount: 2700,
			maxCount: 3000,
		};
		const res = await request(server).post("/api/records").send(req);
		expect(res.status).toBe(200);
		expect(res.body.code).toBe(0);
	});
	it("should return the records as null when correct request body is passed but no records are found", async () => {
		const req = {
			startDate: "2022-01-26",
			endDate: "2023-02-02",
			minCount: 2700,
			maxCount: 3000,
		};
		const res = await request(server).post("/api/records").send(req);
		expect(res.status).toBe(200);
		expect(res.body.records).toBeNull();
	});
});
