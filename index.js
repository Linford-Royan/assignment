const express = require("express");
const app = express();
const config = require("config");

require("./startUp/routes")(app);
require("./startUp/db")();
require("express-async-errors");
require("./startUp/prod")(app);

const port = process.env.PORT || config.get("port") || 5000;
const server = app.listen(port, () => console.log(`Listening on port ${port}...`));
module.exports = server;
