const express = require("express");
const router = express.Router();
const { asyncHandler } = require("../middleware/error");
const { recordsController } = require("../controllers/records-controller");

/* 
POST Method: Accepts for query params
startDate,endDate,minCount,maxCount
1.“startDate” and “endDate” fields will contain the date in a “YYYY-MM-DD” format
2.“minCount” and “maxCount” are for filtering the data. Sum of the “count” array in
the documents should be between “minCount” and “maxCount
Ex:
"startDate": "2016-01-26",
"endDate": "2018-02-02",
"minCount": 2700,
"maxCount": 3000
*/
router.post("/", asyncHandler(recordsController));

module.exports = router;
