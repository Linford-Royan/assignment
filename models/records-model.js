const mongoose = require("mongoose");
const joi = require("joi");

// Model and schema definition for the records
const Records = mongoose.model(
	"Records",
	new mongoose.Schema({
		key: {
			type: String,
		},
		createdAt: {
			type: Date,
		},
		counts: {
			type: Array,
		},
		value: {
			type: String,
		},
	}),
	"records"
);

const schema = joi.object({
	startDate: joi.date().required().less(joi.ref("endDate")),
	endDate: joi.date().required(),
	minCount: joi.number().required().less(joi.ref("maxCount")),
	maxCount: joi.number().required(),
});

// Function to validate the request query params.
// Handles the checks for :
// 1.Mandatory fields
// 2.Incorrect date and count ranges.
function validate(req) {
	const result = schema.validate(req);
	return result;
}

exports.Records = Records;
exports.validate = validate;
